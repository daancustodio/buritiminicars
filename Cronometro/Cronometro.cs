﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cronometro
{
    public partial class Cronometro : Form
    {
        int milesimos = 0, segundos = 0, minutos = 0;
        int milesimos2 = 0, segundos2 = 0, minutos2 = 0;
        int milesimos3 = 0, segundos3 = 0, minutos3 = 0;
        int milesimos4 = 0, segundos4 = 0, minutos4 = 0;
        int milesimos5 = 0, segundos5 = 0, minutos5 = 0;
        int milesimos6 = 0, segundos6 = 0, minutos6 = 0;
        static decimal TaxaPorMinuto = 2.00m; //Taxa a ser multiplicada por cada minuto
        static decimal TaxaPorMinuto2 = 2.00m; //Taxa a ser multiplicada por cada minuto
        static decimal TaxaPorMinuto3 = 2.00m; //Taxa a ser multiplicada por cada minuto
        static decimal TaxaPorMinuto4 = 2.00m; //Taxa a ser multiplicada por cada minuto
        static decimal TaxaPorMinuto5 = 2.00m; //Taxa a ser multiplicada por cada minuto
        static decimal TaxaPorMinuto6 = 2.00m; //Taxa a ser multiplicada por cada minuto


        public Cronometro(string carro)
        {
            InitializeComponent();
            this.Text = carro;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }


        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            lbl_cronometro.Text = "00:00:00";
            milesimos = 0;
            segundos = 0;
            minutos = 0;

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            milesimos++;
            if (milesimos == 60)
            {
                segundos++;
                milesimos = 0;
            }
            else if (segundos == 60)
            {
                minutos++;
                segundos = 0;
            }
            lbl_cronometro.Text = minutos.ToString().PadLeft(2, '0') + ":" + segundos.ToString().PadLeft(2, '0') + ":" + milesimos.ToString().PadLeft(2, '0');
            this.labelValor.Text = "R$: " + this.ObtemValor();

        }

        private string ObtemValor()
        {
            var multiplicador = this.ObtemMultiplicador();
            var valor = TaxaPorMinuto * multiplicador;
            var valorFormatado = Math.Round(valor, 2);

            return valorFormatado.ToString();


        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            milesimos2++;
            if (milesimos2 == 60)
            {
                segundos2++;
                milesimos2 = 0;
            }
            else if (segundos2 == 60)
            {
                minutos2++;
                segundos2 = 0;
            }
            lbl_cronometro2.Text = minutos2.ToString().PadLeft(2, '0') + ":" + segundos2.ToString().PadLeft(2, '0') + ":" + milesimos2.ToString().PadLeft(2, '0');
            this.labelValor2.Text = "R$: " + this.ObtemValor2();

        }

        private string ObtemValor2()
        {
            var multiplicador2 = this.ObtemMultiplicador2();
            var valor2 = TaxaPorMinuto2 * multiplicador2;
            var valorFormatado2 = Math.Round(valor2, 2);

            return valorFormatado2.ToString();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            timer2.Enabled = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            timer2.Enabled = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            timer2.Enabled = false;
            lbl_cronometro2.Text = "00:00:00";
            milesimos2 = 0;
            segundos2 = 0;
            minutos2 = 0;
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            milesimos3++;
            if (milesimos3 == 60)
            {
                segundos3++;
                milesimos3 = 0;
            }
            else if (segundos3 == 60)
            {
                minutos3++;
                segundos3 = 0;
            }
            lbl_cronometro3.Text = minutos3.ToString().PadLeft(2, '0') + ":" + segundos3.ToString().PadLeft(2, '0') + ":" + milesimos3.ToString().PadLeft(2, '0');
            this.labelValor3.Text = "R$: " + this.ObtemValor3();

        }

        private string ObtemValor3()
        {
            var multiplicador3 = this.ObtemMultiplicador3();
            var valor3 = TaxaPorMinuto3 * multiplicador3;
            var valorFormatado = Math.Round(valor3, 2);

            return valorFormatado.ToString();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            timer3.Enabled = true;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            timer3.Enabled = false;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            timer3.Enabled = false;
            lbl_cronometro3.Text = "00:00:00";
            milesimos3 = 0;
            segundos3 = 0;
            minutos3 = 0;
        }

        private void button20_Click(object sender, EventArgs e)
        {
            timer4.Enabled = true;
        }

        private void button19_Click(object sender, EventArgs e)
        {
            timer4.Enabled = false;
        }

        private void button18_Click(object sender, EventArgs e)
        {
            timer4.Enabled = false;
            lbl_cronometro4.Text = "00:00:00";
            milesimos4 = 0;
            segundos4 = 0;
            minutos4 = 0;
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            milesimos4++;
            if (milesimos4 == 60)
            {
                segundos4++;
                milesimos4 = 0;
            }
            else if (segundos4 == 60)
            {
                minutos4++;
                segundos4 = 0;
            }
            lbl_cronometro4.Text = minutos4.ToString().PadLeft(2, '0') + ":" + segundos4.ToString().PadLeft(2, '0') + ":" + milesimos4.ToString().PadLeft(2, '0');
            this.labelValor4.Text = "R$: " + this.ObtemValor4();

        }

        private string ObtemValor4()
        {
            var multiplicador4 = this.ObtemMultiplicador4();
            var valor4 = TaxaPorMinuto4 * multiplicador4;
            var valorFormatado = Math.Round(valor4, 2);

            return valorFormatado.ToString();
        }

        private void timer5_Tick(object sender, EventArgs e)
        {
            milesimos5++;
            if (milesimos5 == 60)
            {
                segundos5++;
                milesimos5 = 0;
            }
            else if (segundos5 == 60)
            {
                minutos5++;
                segundos5 = 0;
            }
            lbl_cronometro5.Text = minutos5.ToString().PadLeft(2, '0') + ":" + segundos5.ToString().PadLeft(2, '0') + ":" + milesimos5.ToString().PadLeft(2, '0');
            this.labelValor5.Text = "R$: " + this.ObtemValor5();

        }

        private string ObtemValor5()
        {
            var multiplicador5 = this.ObtemMultiplicador5();
            var valor5 = TaxaPorMinuto5 * multiplicador5;
            var valorFormatado = Math.Round(valor5, 2);

            return valorFormatado.ToString();
        }

        private void timer6_Tick(object sender, EventArgs e)
        {
            milesimos6++;
            if (milesimos6 == 60)
            {
                segundos6++;
                milesimos6 = 0;
            }
            else if (segundos6 == 60)
            {
                minutos6++;
                segundos6 = 0;
            }
            lbl_cronometro6.Text = minutos6.ToString().PadLeft(2, '0') + ":" + segundos6.ToString().PadLeft(2, '0') + ":" + milesimos6.ToString().PadLeft(2, '0');
            this.labelValor6.Text = "R$: " + this.ObtemValor6();

        }

        private string ObtemValor6()
        {
            var multiplicador6 = this.ObtemMultiplicador6();
            var valor6 = TaxaPorMinuto6 * multiplicador6;
            var valorFormatado = Math.Round(valor6, 2);

            return valorFormatado.ToString();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            timer5.Enabled = true;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            timer5.Enabled = false;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            timer5.Enabled = false;
            lbl_cronometro5.Text = "00:00:00";
            milesimos5 = 0;
            segundos5 = 0;
            minutos5 = 0;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            timer6.Enabled = true;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            timer6.Enabled = false;
        }

        private void labelValor_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            timer6.Enabled = false;
            lbl_cronometro6.Text = "00:00:00";
            milesimos6 = 0;
            segundos6 = 0;
            minutos6 = 0;
        }

        private void labelValor4_Click(object sender, EventArgs e)
        {

        }

        private void lbl_cronometro4_Click(object sender, EventArgs e)
        {

        }

        private void lbl_cronometro_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// ´método que aplica regra de negócio
        /// </summary>
        /// <returns></returns>
        private int ObtemMultiplicador()
        {            
            var regraNegocioSegundos = this.segundos > 0 ? 1 : 0; //regar 01 caso tenha passado mais de um segundo, será cobrado o minuto completo
            var minutosPassados = this.minutos; //Pega o numero de segundos minutos atual;

            var multiplicador = regraNegocioSegundos + minutosPassados; // somatoria dos valores para obter o multiplicador da taxa;

            return multiplicador; 
        }
        private int ObtemMultiplicador2()
        {
            var regraNegocioSegundos2 = this.segundos2 > 0 ? 1 : 0; //regar 01 caso tenha passado mais de um segundo, será cobrado o minuto completo
            var minutosPassados2 = this.minutos2; //Pega o numero de segundos minutos atual;

            var multiplicador2 = regraNegocioSegundos2 + minutosPassados2; // somatoria dos valores para obter o multiplicador da taxa;

            return multiplicador2;
        }
        private int ObtemMultiplicador3()
        {
            var regraNegocioSegundos3 = this.segundos3 > 0 ? 1 : 0; //regar 01 caso tenha passado mais de um segundo, será cobrado o minuto completo
            var minutosPassados3 = this.minutos3; //Pega o numero de segundos minutos atual;

            var multiplicador3 = regraNegocioSegundos3 + minutosPassados3; // somatoria dos valores para obter o multiplicador da taxa;

            return multiplicador3;
        }
        private int ObtemMultiplicador4()
        {
            var regraNegocioSegundos4 = this.segundos4 > 0 ? 1 : 0; //regar 01 caso tenha passado mais de um segundo, será cobrado o minuto completo
            var minutosPassados4 = this.minutos4; //Pega o numero de segundos minutos atual;

            var multiplicador4 = regraNegocioSegundos4 + minutosPassados4; // somatoria dos valores para obter o multiplicador da taxa;

            return multiplicador4;
        }
        private int ObtemMultiplicador5()
        {
            var regraNegocioSegundos5 = this.segundos5 > 0 ? 1 : 0; //regar 01 caso tenha passado mais de um segundo, será cobrado o minuto completo
            var minutosPassados5 = this.minutos5; //Pega o numero de segundos minutos atual;

            var multiplicador5 = regraNegocioSegundos5 + minutosPassados5; // somatoria dos valores para obter o multiplicador da taxa;

            return multiplicador5;
        }
        private int ObtemMultiplicador6()
        {
            var regraNegocioSegundos6 = this.segundos6 > 0 ? 1 : 0; //regar 01 caso tenha passado mais de um segundo, será cobrado o minuto completo
            var minutosPassados6 = this.minutos6; //Pega o numero de segundos minutos atual;

            var multiplicador6 = regraNegocioSegundos6 + minutosPassados6; // somatoria dos valores para obter o multiplicador da taxa;

            return multiplicador6;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.DestroyHandle();
        }

        public static implicit operator Cronometro(bool v)
        {
            throw new NotImplementedException();
        }

        
    }
}
