﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cronometro
{
    public partial class QuiosqueCars : Form
    {
        private Cronometro FormularioCronometro;
        
        public QuiosqueCars()
        {
            InitializeComponent();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            if(this.FormularioCronometro == null)
            {
                 this.FormularioCronometro = new Cronometro("Mini Cars Buriti Shopping");
                 FormularioCronometro.Show();
                 FormularioCronometro.HandleDestroyed += FormularioCronometro_HandleDestroyed;
            }
            else
            {
                this.FormularioCronometro.WindowState = FormWindowState.Maximized;
            }           
        }

        private void FormularioCronometro_HandleDestroyed(object sender, EventArgs e)
        {
            this.FormularioCronometro = null;
        }
    }
}
